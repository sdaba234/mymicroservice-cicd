param (
    [string]$zipFilePath,
    [string]$destinationDirectory
)

# Function to log messages
function Log {
    param (
        [string]$message
    )
    Write-Host "$(Get-Date -Format 'yyyy-MM-dd HH:mm:ss') - $message"
}

Log "Extracting the ZIP file $zipFilePath to $destinationDirectory"
if (Test-Path -Path $destinationDirectory) {
    Remove-Item -Path $destinationDirectory -Recurse -Force
}
New-Item -Path $destinationDirectory -ItemType Directory -Force

Add-Type -AssemblyName System.IO.Compression.FileSystem
[System.IO.Compression.ZipFile]::ExtractToDirectory($zipFilePath, $destinationDirectory)

Log "Extraction completed to $destinationDirectory"