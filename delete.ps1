param (
    [string]$destinationDirectory
)

# Function to log messages
function Log {
    param (
        [string]$message
    )
    Write-Host "$(Get-Date -Format 'yyyy-MM-dd HH:mm:ss') - $message"
}

# Function to delete zip files
function Delete-ZipFiles {
    param (
        [string]$directory
    )

    $zipFiles = Get-ChildItem -Path $directory -Filter *.zip

    if ($zipFiles.Count -eq 0) {
        Log "No zip files found in $directory"
        return
    }

    Log "Found $($zipFiles.Count) zip files in $directory"

    foreach ($zipFile in $zipFiles) {
        Log "Deleting $($zipFile.FullName)"
        Remove-Item -Path $zipFile.FullName -Force
    }

    # Verify deletion
    $remainingZipFiles = Get-ChildItem -Path $directory -Filter *.zip
    if ($remainingZipFiles.Count -eq 0) {
        Log "All zip files have been deleted successfully."
    } else {
        Log "Error: Some zip files were not deleted:"
        foreach ($remainingZipFile in $remainingZipFiles) {
            Log $remainingZipFile.FullName
        }
        exit 1
    }
}

# Delete zip files in the destination directory
Delete-ZipFiles -directory $destinationDirectory