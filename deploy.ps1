param (
    [string]$environment,
    [string]$artifactUrl
)

# Define environment-specific settings
switch ($environment) {
    'dev' {
        $servers = @('DevServer1', 'DevServer2')
        $appPoolName = 'DevAppPool'
        $appPhysicalDirectory = 'C:\inetpub\wwwroot\DevApp'
        $backUpDirectory = 'C:\Backups\DevApp'
        $webSiteName = 'DevWebsite'
        $windowsRuntimePath = 'C:\Path\To\Runtime'
        $destinationDirectory = 'C:\Deployments\DevApp'
        $unzipPath = 'C:\Unzipped\DevApp'
    }
    'qa' {
        $servers = @('QAServer1', 'QAServer2')
        $appPoolName = 'QAAppPool'
        $appPhysicalDirectory = 'C:\inetpub\wwwroot\QAApp'
        $backUpDirectory = 'C:\Backups\QAApp'
        $webSiteName = 'QAWebsite'
        $windowsRuntimePath = 'C:\Path\To\Runtime'
        $destinationDirectory = 'C:\Deployments\QAApp'
        $unzipPath = 'C:\Unzipped\QAApp'
    }
    'prod' {
        $servers = @('ProdServer1', 'ProdServer2')
        $appPoolName = 'ProdAppPool'
        $appPhysicalDirectory = 'C:\inetpub\wwwroot\ProdApp'
        $backUpDirectory = 'C:\Backups\ProdApp'
        $webSiteName = 'ProdWebsite'
        $windowsRuntimePath = 'C:\Path\To\Runtime'
        $destinationDirectory = 'C:\Deployments\ProdApp'
        $unzipPath = 'C:\Unzipped\ProdApp'
    }
    default {
        throw "Unknown environment: $environment"
    }
)

# Function to log messages
function Log {
    param (
        [string]$message
    )
    Write-Host "$(Get-Date -Format 'yyyy-MM-dd HH:mm:ss') - $message"
}

# Define the sequential tasks for each server
function DeployToServer {
    param (
        [string]$server
    )

    Log "Starting deployment on server $server"

    # Step 1: Delete previous artifacts
    Invoke-Command -ComputerName $server -ScriptBlock {
        param ($destinationDirectory)
        .\delete.ps1 -destinationDirectory $using:destinationDirectory
    } -ArgumentList $destinationDirectory

    # Step 2: Take a backup of the machine
    Invoke-Command -ComputerName $server -ScriptBlock {
        param ($appPhysicalDirectory, $backUpDirectory)
        .\backup.ps1 -appPhysicalDirectory $using:appPhysicalDirectory -backUpDirectory $using:backUpDirectory
    } -ArgumentList $appPhysicalDirectory, $backUpDirectory

    # Step 3: Download the new artifact
    Invoke-Command -ComputerName $server -ScriptBlock {
        param ($artifactUrl, $destinationDirectory)
        .\download.ps1 -artifactUrl $using:artifactUrl -destinationDirectory $using:destinationDirectory
    } -ArgumentList $artifactUrl, $destinationDirectory

    # Step 4: Stop the application pool
    Invoke-Command -ComputerName $server -ScriptBlock {
        param ($appPoolName)
        .\stop.ps1 -appPoolName $using:appPoolName
    } -ArgumentList $appPoolName

    # Step 5: Extract the artifact
    Invoke-Command -ComputerName $server -ScriptBlock {
        param ($zipFilePath, $unzipPath)
        .\extract.ps1 -zipFilePath $using:zipFilePath -unzipPath $using:unzipPath
    } -ArgumentList "$destinationDirectory\artifact.zip", $unzipPath

    # Step 6: Start the application pool
    Invoke-Command -ComputerName $server -ScriptBlock {
        param ($appPoolName)
        .\start.ps1 -appPoolName $using:appPoolName
    } -ArgumentList $appPoolName

    # Step 7: Validate the application pool
    Invoke-Command -ComputerName $server -ScriptBlock {
        param ($appPoolName)
        .\validate.ps1 -appPoolName $using:appPoolName
    } -ArgumentList $appPoolName

    Log "Completed deployment on server $server"
}

# Iterate over each server and deploy sequentially
foreach ($server in $servers) {
    DeployToServer -server $server
}