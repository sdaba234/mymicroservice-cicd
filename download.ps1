param (
    [string]$artifactUrl,
    [string]$destinationDirectory
)

# Function to log messages
function Log {
    param (
        [string]$message
    )
    Write-Host "$(Get-Date -Format 'yyyy-MM-dd HH:mm:ss') - $message"
}

Log "Downloading new artifact from $artifactUrl"
if (-not (Test-Path $destinationDirectory)) {
    New-Item -ItemType Directory -Path $destinationDirectory
}
$localArtifactPath = Join-Path $destinationDirectory "artifact.zip"
Invoke-WebRequest -Uri $artifactUrl -OutFile $localArtifactPath

Log "Downloaded artifact to $localArtifactPath"