trigger:
- main

variables:
  buildConfiguration: 'Release'
  artifactName: 'drop'
  artifactFeed: 'your-feed-name'  # Replace with your actual Azure Artifacts feed name
  sonarqubeProjectKey: 'your-sonarqube-project-key'  # Replace with your SonarQube project key
  sonarqubeProjectName: 'your-sonarqube-project-name'  # Replace with your SonarQube project name
  sonarqubeProjectVersion: '1.0'

stages:
- stage: Build
  displayName: 'Build Stage'
  jobs:
  - job: Build
    displayName: 'Build Job'
    pool:
      vmImage: 'windows-latest'
    steps:
    - task: UseDotNet@2
      inputs:
        packageType: 'sdk'
        version: '6.x'  # Use the appropriate version for your project
    - task: SonarQubePrepare@5
      inputs:
        SonarQube: 'SonarQubeServiceConnection'
        scannerMode: 'MSBuild'
        projectKey: $(sonarqubeProjectKey)
        projectName: $(sonarqubeProjectName)
        projectVersion: $(sonarqubeProjectVersion)
    - task: VSBuild@1
      inputs:
        solution: '**/*.sln'
        msbuildArgs: '/p:DeployOnBuild=true /p:WebPublishMethod=Package /p:PackageAsSingleFile=true /p:SkipInvalidConfigurations=true /p:PackageLocation="$(Build.ArtifactStagingDirectory)\App.zip"'
        platform: '$(buildPlatform)'
        configuration: '$(buildConfiguration)'
    - task: VSTest@2
      inputs:
        platform: '$(buildPlatform)'
        configuration: '$(buildConfiguration)'
    - task: SonarQubeAnalyze@5
    - task: SonarQubePublish@5
      inputs:
        pollingTimeoutSec: '300'
    - task: PublishBuildArtifacts@1
      inputs:
        PathtoPublish: '$(Build.ArtifactStagingDirectory)'
        ArtifactName: '$(artifactName)'
        publishLocation: 'Container'
    - task: UniversalPackages@0
      inputs:
        command: 'publish'
        publishDirectory: '$(Build.ArtifactStagingDirectory)'
        feedsToUse: 'internal'
        vstsFeedPublish: '$(artifactFeed)'
        versionOption: 'patch'
        
- stage: DeployDev
  displayName: 'Deploy to Dev Environment'
  dependsOn: Build
  jobs:
  - deployment: DeployToDev
    displayName: 'Deploy to Dev'
    environment: 'Dev'
    pool:
      vmImage: 'windows-latest'
    strategy:
      runOnce:
        deploy:
          steps:
          - download: current
            artifact: $(artifactName)
          - script: |
              param (
                [string]$environment,
                [string]$artifactUrl
              )

              $servers = @('DevServer1', 'DevServer2')
              $appPoolName = 'DevAppPool'
              $appPhysicalDirectory = 'C:\inetpub\wwwroot\DevApp'
              $backUpDirectory = 'C:\Backups\DevApp'
              $destinationDirectory = 'C:\Deployments\DevApp'
              $unzipPath = 'C:\Unzipped\DevApp'

              # Function to log messages
              function Log {
                  param (
                      [string]$message
                  )
                  Write-Host "$(Get-Date -Format 'yyyy-MM-dd HH:mm:ss') - $message"
              }

              # Define the sequential tasks for each server
              function DeployToServer {
                  param (
                      [string]$server
                  )

                  Log "Starting deployment on server $server"

                  # Step 1: Delete previous artifacts
                  Invoke-Command -ComputerName $server -ScriptBlock {
                      param ($destinationDirectory)
                      .\delete.ps1 -destinationDirectory $using:destinationDirectory
                  } -ArgumentList $destinationDirectory

                  # Step 2: Take a backup of the machine
                  Invoke-Command -ComputerName $server -ScriptBlock {
                      param ($appPhysicalDirectory, $backUpDirectory)
                      .\backup.ps1 -appPhysicalDirectory $using:appPhysicalDirectory -backUpDirectory $using:backUpDirectory
                  } -ArgumentList $appPhysicalDirectory, $backUpDirectory

                  # Step 3: Download the new artifact
                  Invoke-Command -ComputerName $server -ScriptBlock {
                      param ($artifactUrl, $destinationDirectory)
                      .\download.ps1 -artifactUrl $using:artifactUrl -destinationDirectory $using:destinationDirectory
                  } -ArgumentList $artifactUrl, $destinationDirectory

                  # Step 4: Stop the application pool
                  Invoke-Command -ComputerName $server -ScriptBlock {
                      param ($appPoolName)
                      .\stop.ps1 -appPoolName $using:appPoolName
                  } -ArgumentList $appPoolName

                  # Step 5: Extract the artifact
                  Invoke-Command -ComputerName $server -ScriptBlock {
                      param ($zipFilePath, $unzipPath)
                      .\extract.ps1 -zipFilePath $using:zipFilePath -unzipPath $using:unzipPath
                  } -ArgumentList "$destinationDirectory\artifact.zip", $unzipPath

                  # Step 6: Start the application pool
                  Invoke-Command -ComputerName $server -ScriptBlock {
                      param ($appPoolName)
                      .\start.ps1 -appPoolName $using:appPoolName
                  } -ArgumentList $appPoolName

                  # Step 7: Validate the application pool
                  Invoke-Command -ComputerName $server -ScriptBlock {
                      param ($appPoolName)
                      .\validate.ps1 -appPoolName $using:appPoolName
                  } -ArgumentList $appPoolName

                  Log "Completed deployment on server $server"
              }

              # Iterate over each server and deploy sequentially
              foreach ($server in $servers) {
                  DeployToServer -server $server
              }

            displayName: 'Deploy to Dev Servers'
            failOnStderr: true

- stage: DeployQA
  displayName: 'Deploy to QA Environment'
  dependsOn: DeployDev
  jobs:
  - deployment: DeployToQA
    displayName: 'Deploy to QA'
    environment: 'QA'
    pool:
      vmImage: 'windows-latest'
    strategy:
      runOnce:
        deploy:
          steps:
          - download: current
            artifact: $(artifactName)
          - script: |
              param (
                [string]$environment,
                [string]$artifactUrl
              )

              $servers = @('QAServer1', 'QAServer2')
              $appPoolName = 'QAAppPool'
              $appPhysicalDirectory = 'C:\inetpub\wwwroot\QAApp'
              $backUpDirectory = 'C:\Backups\QAApp'
              $destinationDirectory = 'C:\Deployments\QAApp'
              $unzipPath = 'C:\Unzipped\QAApp'

              # Function to log messages
              function Log {
                  param (
                      [string]$message
                  )
                  Write-Host "$(Get-Date -Format 'yyyy-MM-dd HH:mm:ss') - $message"
              }

              # Define the sequential tasks for each server
              function DeployToServer {
                  param (
                      [string]$server
                  )

                  Log "Starting deployment on server $server"

                  # Step 1: Delete previous artifacts
                  Invoke-Command -ComputerName $server -ScriptBlock {
                      param ($destinationDirectory)
                      .\delete.ps1 -destinationDirectory $using:destinationDirectory
                  } -ArgumentList $destinationDirectory

                  # Step 2: Take a backup of the machine
                  Invoke-Command -ComputerName $server -ScriptBlock {
                      param ($appPhysicalDirectory, $backUpDirectory)
                      .\backup.ps1 -appPhysicalDirectory $using:appPhysicalDirectory -backUpDirectory $using:backUpDirectory
                  } -ArgumentList $appPhysicalDirectory, $backUpDirectory

                  # Step 3: Download the new artifact
                  Invoke-Command -ComputerName $server -ScriptBlock {
                      param ($artifactUrl, $destinationDirectory)
                      .\download.ps1 -artifactUrl $using:artifactUrl -destinationDirectory $using:destinationDirectory
                  } -ArgumentList $artifactUrl, $destinationDirectory

                  # Step 4: Stop the application pool
                  Invoke-Command -ComputerName $server -ScriptBlock {
                      param ($appPoolName)
                      .\stop.ps1 -appPoolName $using:appPoolName
                  } -ArgumentList $appPoolName

                  # Step 5: Extract the artifact
                  Invoke-Command -ComputerName $server -ScriptBlock {
                      param ($zipFilePath, $unzipPath)
                      .\extract.ps1 -zipFilePath $using:zipFilePath -unzipPath $using:unzipPath
                  } -ArgumentList "$destinationDirectory\artifact.zip", $unzipPath

                  # Step 6: Start the application pool
                  Invoke-Command -ComputerName $server -ScriptBlock {
                      param ($appPoolName)
                      .\start.ps1 -appPoolName $using:appPoolName
                  } -ArgumentList $appPoolName

                  # Step 7: Validate the application pool
                  Invoke-Command -ComputerName $server -ScriptBlock {
                      param ($appPoolName)
                      .\validate.ps1 -appPoolName $using:appPoolName
                  } -ArgumentList $appPoolName

                  Log "Completed deployment on server $server"
              }

              # Iterate over each server and deploy sequentially
              foreach ($server in $servers) {
                  DeployToServer -server $server
              }

            displayName: 'Deploy to QA Servers'
            failOnStderr: true
    environment:
      name: 'QA'
      resourceType: VirtualMachine
      tags: qa
      protectionLevel: approval
    approvals:
      - type: 'manual'
        requester: 'your-username'  # Replace with actual username

- stage: DeployProd
  displayName: 'Deploy to Prod Environment'
  dependsOn: DeployQA
  jobs:
displayName: 'Deploy to Prod'
environment: 'Prod'
pool:
  vmImage: 'windows-latest'
strategy:
  runOnce:
    deploy:
      steps:
      - download: current
        artifact: $(artifactName)
      - script: |
          param (
            [string]$environment,
            [string]$artifactUrl
          )

          $servers = @('ProdServer1', 'ProdServer2')
          $appPoolName = 'ProdAppPool'
          $appPhysicalDirectory = 'C:\inetpub\wwwroot\ProdApp'
          $backUpDirectory = 'C:\Backups\ProdApp'
          $destinationDirectory = 'C:\Deployments\ProdApp'
          $unzipPath = 'C:\Unzipped\ProdApp'

          # Function to log messages
          function Log {
              param (
                  [string]$message
              )
              Write-Host "$(Get-Date -Format 'yyyy-MM-dd HH:mm:ss') - $message"
          }

          # Define the sequential tasks for each server
          function DeployToServer {
              param (
                  [string]$server
              )

              Log "Starting deployment on server $server"

              # Step 1: Delete previous artifacts
              Invoke-Command -ComputerName $server -ScriptBlock {
                  param ($destinationDirectory)
                  .\delete.ps1 -destinationDirectory $using:destinationDirectory
              } -ArgumentList $destinationDirectory

              # Step 2: Take a backup of the machine
              Invoke-Command -ComputerName $server -ScriptBlock {
                  param ($appPhysicalDirectory, $backUpDirectory)
                  .\backup.ps1 -appPhysicalDirectory $using:appPhysicalDirectory -backUpDirectory $using:backUpDirectory
              } -ArgumentList $appPhysicalDirectory, $backUpDirectory

              # Step 3: Download the new artifact
              Invoke-Command -ComputerName $server -ScriptBlock {
                  param ($artifactUrl, $destinationDirectory)
                  .\download.ps1 -artifactUrl $using:artifactUrl -destinationDirectory $using:destinationDirectory
              } -ArgumentList $artifactUrl, $destinationDirectory

              # Step 4: Stop the application pool
              Invoke-Command -ComputerName $server -ScriptBlock {
                  param ($appPoolName)
                  .\stop.ps1 -appPoolName $using:appPoolName
              } -ArgumentList $appPoolName

              # Step 5: Extract the artifact
              Invoke-Command -ComputerName $server -ScriptBlock {
                  param ($zipFilePath, $unzipPath)
                  .\extract.ps1 -zipFilePath $using:zipFilePath -unzipPath $using:unzipPath
              } -ArgumentList "$destinationDirectory\artifact.zip", $unzipPath

              # Step 6: Start the application pool
              Invoke-Command -ComputerName $server -ScriptBlock {
                  param ($appPoolName)
                  .\start.ps1 -appPoolName $using:appPoolName
              } -ArgumentList $appPoolName

              # Step 7: Validate the application pool
              Invoke-Command -ComputerName $server -ScriptBlock {
                  param ($appPoolName)
                  .\validate.ps1 -appPoolName $using:appPoolName
              } -ArgumentList $appPoolName

              Log "Completed deployment on server $server"
          }

          # Iterate over each server and deploy sequentially
          foreach ($server in $servers) {
              DeployToServer -server $server
          }

        displayName: 'Deploy to Prod Servers'
        failOnStderr: true
environment:
  name: 'Prod'
  resourceType: VirtualMachine
  tags: prod
  protectionLevel: approval
approvals:
  - type: 'manual'
    requester: 'your-username'  # Replace with actual username  - deployment: DeployToProd
