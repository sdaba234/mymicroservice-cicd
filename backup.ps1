param (
    [string]$appPhysicalDirectory,
    [string]$backUpDirectory
)

# Function to log messages
function Log {
    param (
        [string]$message
    )
    Write-Host "$(Get-Date -Format 'yyyy-MM-dd HH:mm:ss') - $message"
}

Log "Taking backup of the current state"
if (-not (Test-Path $backUpDirectory)) {
    New-Item -ItemType Directory -Path $backUpDirectory
}
$timestamp = Get-Date -Format "yyyyMMddHHmmss"
$backupFolder = Join-Path $backUpDirectory $timestamp
Copy-Item -Path $appPhysicalDirectory -Destination $backupFolder -Recurse

Log "Backup completed at $backupFolder"