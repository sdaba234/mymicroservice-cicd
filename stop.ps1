param (
    [string]$appPoolName
)

# Function to log messages
function Log {
    param (
        [string]$message
    )
    Write-Host "$(Get-Date -Format 'yyyy-MM-dd HH:mm:ss') - $message"
}

Log "Checking the state of the application pool $appPoolName"
Import-Module WebAdministration
$appPoolState = Get-WebAppPoolState -Name $appPoolName

if ($appPoolState.Value -eq 'Started') {
    Log "Stopping the application pool $appPoolName"
    Stop-WebAppPool -Name $appPoolName
} else {
    Log "The application pool $appPoolName is already stopped or in a non-started state (current state: $($appPoolState.Value))"
}