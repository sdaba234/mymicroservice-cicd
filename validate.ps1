param (
    [string]$appPoolName
)

# Function to log messages
function Log {
    param (
        [string]$message
    )
    Write-Host "$(Get-Date -Format 'yyyy-MM-dd HH:mm:ss') - $message"
}

Log "Validating the application pool $appPoolName"
Import-Module WebAdministration
$appPoolState = Get-WebAppPoolState -Name $appPoolName

if ($appPoolState.Value -eq 'Started') {
    Log "Application pool $appPoolName is running."
} else {
    Log "Error: Application pool $appPoolName is not running. Status: $($appPoolState.Value)"
    exit 1
}