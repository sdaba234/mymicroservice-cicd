param (
    [string]$appPoolName
)

# Function to log messages
function Log {
    param (
        [string]$message
    )
    Write-Host "$(Get-Date -Format 'yyyy-MM-dd HH:mm:ss') - $message"
}

Log "Starting the application pool $appPoolName"
Import-Module WebAdministration
Start-WebAppPool -Name $appPoolName